# Blueprints for accessing different important arrays for results and performances

# For (unnormalized) joint distribution between quantized neuron and target labels
#    model.layers_P_NY[layer_index][quantizer_index][neuron_index,:,:]
#An (unnormalized) probability value this joint distribution
#    model.layers_P_NY[layer_index][quantizer_index][neuron_index,quantization_level,target_class]

#For mean values of individual neurons 
#    model.layers_mean[layer_index][neuron_index]
#For single mean value of the whole layer
#     model.total_layer_mean[layer_index]

#Information quantities computed for a specific trained model
#     model.layers_entropy[layer_index][quantizer_index][neuron_index]
#     model.layers_MI[layer_index][quantizer_index][neuron_index]
#     model.layers_KL[layer_index][quantizer_index][neuron_index,target_class]
#     model.layers_maxKL[layer_index][quantizer_index][neuron_index]

#Layerwise pruning performance
#     pruning_performance[quantity_index, lowvshigh, layer_index, quantizer_index, number_of_neruons_pruned]

#Pruning all layers together performance
#     pruning_performance[quantity_index,lowvshigh, quantizer_index, number_of_neruons_pruned]

#Layerwise pruning with retraining 
#     epochs_performance[epoch_step_index][quantity_index, lowvshigh, layer_index, quantizer_index, number_of_neruons_pruned]
#Model trained from scratch with reduced size
#     from_scratch_performance[epoch_step_index][layer_index,number_of_neruons_pruned(or reduced size)]


import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt

class FC_network(nn.Module):
    def __init__(self, network_parameters, nonlinearity):
        super().__init__()
        self.nonlinearity = nonlinearity
        self.network_parameters = network_parameters
        self.num_layers = len(network_parameters)-1 #number of fc layers
        # using getattr to dynamically access the class corresponding to nonlinearity passed as input
        self.activation_class = getattr(nn, nonlinearity) 
        self.activation = self.activation_class() 
         # using ModuleList, otherwise these layers won't be registered as network parameters
        self.lin = nn.ModuleList() 
        for i in np.arange(self.num_layers): 
            # Adding fully connected network layers
            self.lin.append(nn.Linear(in_features=self.network_parameters[i],out_features=self.network_parameters[i+1])) 

    def forward(self,x):
        bs= x.shape[0]
         #Flatten the image. 
        x = x.view(bs,self.network_parameters[0])
        #last layer not included in the following loop because we don't apply nonlinearity there
        for layer_index in np.arange(self.num_layers-1):
            x = self.activation(self.lin[layer_index](x))
        x = self.lin[self.num_layers-1](x) #last layer is not in the loop as we don't want to apply non-linearity there
        return x

    # This function is used to compute the unnormalized joint distribution between individual neuron activations and the target labels
    def compute_dist(self,x,target_labels, quantization_levels, layermean=True, mean_multiplier=4):
        bs= x.shape[0] 
        x = x.view(bs,self.network_parameters[0])
        #last layer is not included as we do not apply nonlinearity there.
        for i in np.arange(self.num_layers-1):
            x = self.activation(self.lin[i](x))
            #calculating the unnormalized joint distribution
            for k in np.arange(len(quantization_levels)): #iterating over quantizers         
                for j in np.arange(bs): # iterating over examples in each batch
                    #For different nonlinearities we need to define quantizater differently
                    if self.nonlinearity =='Sigmoid':
                        #For sigmoid we have to remove the quantization level from -infty to 0 as sigmoid doesn't go below zero. Also it doesn't go above 1 so we remove the last level. This is why we have [1:-1]
                        quant_bins = np.linspace(0,1,num=quantization_levels[k]+1)[1:-1] 
                        quantization_indices = np.digitize(x[j,:].cpu().numpy(),quant_bins)
                    elif layermean==True:
                        #we have right=True to have deadzone values separated for ReLU from the rest
                        quantization_indices = np.digitize(x[j,:].cpu().numpy(),np.linspace(0,mean_multiplier*self.total_layer_mean[i],num=quantization_levels[k]-1),right=True)
                    else:
                        #we have right=True to have deadzone values separated for ReLU from the rest
                        #each neuron quantized w.r.t. it's own mean
                        quantization_indices = np.array([np.digitize(x[j,l].cpu().numpy(),np.linspace(0,mean_multiplier*self.layers_mean[i][l],num=quantization_levels[k]-1),right=True) for l in np.arange(x.shape[1])])
                        
                    self.layers_P_NY[i][k][(np.arange(x.shape[1]), quantization_indices, [target_labels[j].item()]*x.shape[1])] +=1 # first index on the L.H.S is for accessing the appropriate multidimensional numpy array from the list for the specific layer and the specific quantizer, then the second set of indices on the L.H.S. is for indexing inside this multidimensional array

        #computations for the last layer. Since last layer has no nonlinearity we simply use uniform quantizer spread above and below mean values. These computations are actually not of much use since there is no pruning from output neurons.
        x = self.lin[self.num_layers-1](x) 
        for k in np.arange(len(quantization_levels)):
            for j in np.arange(bs):
                if layermean==True:
                    margin = abs(self.total_layer_mean[self.num_layers-1])*mean_multiplier/2
                    quantization_indices = np.digitize(x[j,:].cpu().numpy(),np.linspace(self.total_layer_mean[self.num_layers-1]-margin,self.total_layer_mean[self.num_layers-1]+margin,num=quantization_levels[k]-1),right=True) 
                else:
                    quantization_indices = np.array([np.digitize(x[j,l].cpu().numpy(),np.linspace(self.layers_mean[self.num_layers-1][l]-abs(self.layers_mean[self.num_layers-1][l])*mean_multiplier/2,self.layers_mean[self.num_layers-1][l]+abs(self.layers_mean[self.num_layers-1][l])*mean_multiplier/2,num=quantization_levels[k]-1),right=True) for l in np.arange(x.shape[1])])
                
                self.layers_P_NY[self.num_layers-1][k][(np.arange(x.shape[1]), quantization_indices,[target_labels[j].item()]*x.shape[1])] +=1 
        return None
    
    #this function computes the mean activation value of each neuron for the trained network. The mean values are useful for both bias-balancing and ReLU quantization. Use the training set to compute the mean values and not the validation/test set
    def accumulate_mean(self,x):
        bs= x.shape[0]   
        x = x.view(bs,self.network_parameters[0])  
        for layer_index in np.arange(self.num_layers-1):
            x = self.activation(self.lin[layer_index](x))
            # calculating the mean values (without any quantization).
            self.layers_mean[layer_index] += np.sum(x.cpu().numpy(),axis=0) 

        x = self.lin[self.num_layers-1](x) 
        self.layers_mean[self.num_layers-1] += np.sum(x.cpu().numpy(),axis=0)
        return None
    

    
    
    
def train_model(model, train_loader, optimizer, criterion, epochs, device):
    #the function is used to train a DNN model using the arguments provided
    model.to(device)
    model.train()
    
    for epoch in range(epochs):  # loop over the dataset multiple times
        for batch_id, data in enumerate(train_loader, 0):
            # get the inputs
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            
    return None



def calc_acc(model, data_loader, device):
    model.to(device)
    model.eval()
    with torch.no_grad(): 
        acc =0
        for batch_id, data in enumerate(data_loader,0):
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            outputs = model(inputs)
            _,predicted = torch.max(outputs.data, 1)
            acc += (predicted==labels).sum().item()
    return acc



def calc_mean(model,data_loader, device):
    import numpy as np
    model.to(device)
    model.eval()
    
    '''initializing the data_structure to store mean values. The structure is as follows. It is a list, where each element of the      list corresponds to one layer. The element itself is a numpy array of the size corresponding to the number of neurons in          that layer'''
    #to store the number mean values for each neuron separately
    model.layers_mean = []
    for i in np.arange(model.num_layers):
        model.layers_mean.append(np.zeros(model.lin[i].weight.shape[0]))
    
    #to store the one mean value for a whole FC layer by averaging over all the neurons in the layer
    model.total_layer_mean = np.zeros(model.num_layers)
    with torch.no_grad():
        #accumulating values over the dataset for each neuron
        for batch_id, data in enumerate(data_loader,0):
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            model.accumulate_mean(inputs)
        '''normalize the accumulated values by the size of dataset to get the mean of each neuron. Then average over the layer to get the mean of each layer'''
        for i in np.arange(model.num_layers):
            model.layers_mean[i] = model.layers_mean[i]/len(data_loader.dataset)   
            model.total_layer_mean[i] = np.sum(model.layers_mean[i])/np.size(model.layers_mean[i])
    return None



def calc_joint_dist(model, data_loader, quantization_levels, device, layermean=True, mean_multiplier=4):
    import numpy as np
    model.to(device)
    model.eval()
    
    '''Initializing the datastructure holding the unnormalized distribution. We have a list, where each element corresponds to a specific layer. The element itself is again a list, where each element inside this list corresponds to a specific quantizer. The elements inside this second list are join distributions of all the neurons of the specific layer for the specific quantizer packed into one numpy array. So it these numpy arrays have three dimensions-> neuron_index, quantization_level, target_label'''
    model.layers_P_NY = []
    for i in np.arange(model.num_layers):
        model.layers_P_NY.append([])
        for j in np.arange(len(quantization_levels)): #initializing the elements of inner list for each quantizer setting 
            model.layers_P_NY[i].append(np.zeros((model.lin[i].weight.shape[0], quantization_levels[j], len(torch.unique(torch.Tensor(data_loader.dataset.dataset.train_labels))))))
    
    with torch.no_grad():         
        for batch_id, data in enumerate(data_loader,0):
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            model.compute_dist(inputs, labels, quantization_levels, layermean, mean_multiplier)
    return None


def quant_entropy(marginal_count):
    '''We are provided with an unnormalized distribution. The function can handle multidimensional array specifying a joint distribution but for our purposes one should stick to feeding a 1D numpy array. Also make sure that there are no negative values in the unnormalized distribution before passing to this function. We do not check for that.'''
    marginal_dist = marginal_count/np.sum(marginal_count)
    #removing zeros in order to avoid un
    marginal_dist = marginal_dist[np.nonzero(marginal_dist)]
    entropy = -np.sum(marginal_dist*np.log2(marginal_dist))
    return entropy


def quant_MI(joint_count):
    '''This function takes an unnormalized joint distribution in the form of a 2D numpy array and then computes MI by first computing the product distribution of the marginals and then the MI'''
    joint_dist = joint_count/np.sum(joint_count)
    indep_prod_dist = np.outer(np.sum(joint_dist,axis=1),np.sum(joint_dist,axis=0))
    MI = 0
    for i in np.arange(joint_dist.shape[0]):
        for j in np.arange(joint_dist.shape[1]):
            if joint_dist[i,j] !=0:
                MI += joint_dist[i,j]*np.log2(joint_dist[i,j]/indep_prod_dist[i,j])
    return MI


def KL_spectrum(joint_count):
    '''This function takes an unnormalized joint distribution in the form of a 2D array as input. It is important to have the array in the right format: axis 0 is the quantization level axis, axis 1 is the target labels axis. The function returns a KL spectrum over the target labels. The maximization over the KL spectrum is not a part of the function'''
    #margginal distribution over the quantization levels. This forms the second argument of the KL divergence computations (as in the paper)
    marginal_dist = np.sum(joint_count,axis=1)/np.sum(joint_count)
    KL_spectrum = np.zeros(joint_count.shape[1])
    #conditional distributions of quantization levels conditioned on the target label. the axis1 of the cond_dist specifies the target label we want to condition on and the axis0 defines the conditional distribution
    cond_dist = joint_count/np.sum(joint_count,axis=0)
    for i in np.arange(KL_spectrum.size):
        #note that if the conditional distribution has a non-zero for a certain quantization level then the marginal also has a nonzero there so we only check for conditional distribution nonzero elements
        KL_spectrum[i] = np.sum(cond_dist[np.nonzero(cond_dist[:,i]),i]*np.log2(cond_dist[np.nonzero(cond_dist[:,i]),i]/marginal_dist[np.nonzero(cond_dist[:,i])]))
    return KL_spectrum

def calc_model_entropy(model):
    '''This function calculates the entropy for all the neurons in a model and all the different quantization levels used while computing the joint distribution model.layers_P_NY'''
    
    '''the list holding the entropies. Each element of the list is another list. This inner list corresponds to one specific layer. The elements of inner list are numpy arrays. Each numpy array corresponds to a specific row and a specific quantization setting. The numpy array holds the entropy values for the specific setting'''
    model.layers_entropy = []
    #extracting number of quantizers using the joint distribution
    num_quantizers = len(model.layers_P_NY[0])
    for layer_index in np.arange(model.num_layers):
        model.layers_entropy.append([]) 
        for quantizer_index in np.arange(num_quantizers):
            num_neurons = model.layers_P_NY[layer_index][quantizer_index].shape[0]
            model.layers_entropy[layer_index].append(np.zeros(num_neurons))
            for neuron_index in np.arange(num_neurons):
                #extracting the joint distribution over quantization levels and target labels for the specific neuron
                joint_dist = model.layers_P_NY[layer_index][quantizer_index][neuron_index,:,:]
                #to compute entropy over quantization levels we first marginalize over target labels
                model.layers_entropy[layer_index][quantizer_index][neuron_index] = quant_entropy(np.sum(joint_dist, axis=1))
     
    return None


def calc_model_MI(model):
    '''This function calculates the MI for all the neurons in a model and all the different quantization levels using the joint distribution model.layers_P_NY'''
    
    '''the list holding the MI values. Each element of the list is another list. This inner list corresponds to one specific layer. The elements of inner list are numpy arrays. Each numpy array corresponds to a specific row and a specific quantization setting. The numpy array holds the MI values for the specific setting'''
    model.layers_MI = []
    #extracting number of quantizers using the joint distribution
    num_quantizers = len(model.layers_P_NY[0])
    for layer_index in np.arange(model.num_layers):
        model.layers_MI.append([]) 
        for quantizer_index in np.arange(num_quantizers):
            num_neurons = model.layers_P_NY[layer_index][quantizer_index].shape[0]
            model.layers_MI[layer_index].append(np.zeros(num_neurons))
            for neuron_index in np.arange(num_neurons):
                #extracting the joint distribution over quantization levels and target labels for the specific neuron
                joint_dist = model.layers_P_NY[layer_index][quantizer_index][neuron_index,:,:]
                model.layers_MI[layer_index][quantizer_index][neuron_index] = quant_MI(joint_dist)
     
    return None

def calc_model_KLspectrum(model):
    '''This function calculates the KL spectrum for all the neurons in a model and all the different quantization levels using the joint distribution model.layers_P_NY. It also computes the max over the KL spectrum for each neuron.'''
     
    '''the list holding the KL spectrums. Each element of the list is another list. This inner list corresponds to one specific layer. The elements of inner list are numpy arrays. Each numpy array corresponds to a specific row and a specific quantization setting. The numpy array holds the KL spectrums for the specific setting'''
    model.layers_KL = []
    model.layers_maxKL =[]
    #extracting number of quantizers and number of target classes using the joint distribution
    num_quantizers = len(model.layers_P_NY[0])
    num_target_labels  = model.layers_P_NY[0][0].shape[2]
    for layer_index in np.arange(model.num_layers):
        model.layers_KL.append([])
        model.layers_maxKL.append([])
        for quantizer_index in np.arange(num_quantizers):
            num_neurons = model.layers_P_NY[layer_index][quantizer_index].shape[0]
            model.layers_maxKL[layer_index].append(np.zeros(num_neurons))
            model.layers_KL[layer_index].append(np.zeros((num_neurons,num_target_labels)))
            for neuron_index in np.arange(num_neurons):
                #extracting the joint distribution over quantization levels and target labels for the specific neuron
                joint_dist = model.layers_P_NY[layer_index][quantizer_index][neuron_index,:,:]
                model.layers_KL[layer_index][quantizer_index][neuron_index,:] = KL_spectrum(joint_dist)
                model.layers_maxKL[layer_index][quantizer_index][neuron_index] = np.max(model.layers_KL[layer_index][quantizer_index][neuron_index,:])
     
    return None


def copy_model(state_dict, network_parameters, nonlinearity):
    model = FC_network(network_parameters, nonlinearity) 
    model.load_state_dict(state_dict)
    weights =[]
    biases = []
    num_layers = len(network_parameters)-1
    for layer_index in np.arange(num_layers):
        weights.append(model.lin[layer_index].weight.data.clone())
        biases.append(model.lin[layer_index].bias.data.clone())    
    
    return {'weights': weights,
            'biases': biases}

        
def single_layer_resize(state_dict, orig_model, layer_index, remaining_indices, device, network_parameters, nonlinearity, layers_mean=None, bias_balancing=False):
    #initializing the model from scratch so that we don't have residual impacts from previous prunings/retrainings
    model = FC_network(network_parameters, nonlinearity) 
    model.load_state_dict(state_dict)
    #resizing previous weight matrix and bias for the layer in consideration
    model.lin[layer_index] = nn.Linear(in_features=orig_model['weights'][layer_index].shape[1], out_features=remaining_indices.size)
    model.lin[layer_index].weight = nn.Parameter(orig_model['weights'][layer_index][remaining_indices,:])
    model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices])
    
    #resizing the next weight matrix
    model.lin[layer_index+1] = nn.Linear(in_features= remaining_indices.size, out_features=orig_model['weights'][layer_index+1].shape[0])
    model.lin[layer_index+1].weight = nn.Parameter(orig_model['weights'][layer_index+1][:,remaining_indices])
    
    temp = np.arange(orig_model['biases'][layer_index].shape[0])
    #temp1 is basically same as ordered[:i] in the pruning function
    removed_neurons = np.delete(temp, remaining_indices)
    #no bias balancing needed if no neuron was removed from previous layer
    if bias_balancing==False or removed_neurons.size==0:
        model.lin[layer_index+1].bias = nn.Parameter(orig_model['biases'][layer_index+1])
    else:
        model.lin[layer_index+1].bias = nn.Parameter(orig_model['biases'][layer_index+1] + torch.matmul(orig_model['weights'][layer_index+1][:,removed_neurons],torch.from_numpy(layers_mean[layer_index][removed_neurons]).float()))
       
    return model


def resize_network(state_dict, orig_model, remaining_indices_list, device, network_parameters, nonlinearity, layers_mean=False, bias_balancing=False):
    #initializing the model from scratch so that we don't have residual impacts from previous prunings/retrainings
    model = FC_network(network_parameters, nonlinearity) 
    model.load_state_dict(state_dict)
    num_layers = len(network_parameters)-1
    for layer_index in np.arange(num_layers):
        model.lin[layer_index] = nn.Linear(in_features=remaining_indices_list[layer_index].size,out_features=remaining_indices_list[layer_index+1].size)
        model.lin[layer_index].weight = nn.Parameter(orig_model['weights'][layer_index][remaining_indices_list[layer_index+1],:][:,remaining_indices_list[layer_index]])
        
        prev_layer_removed_neurons = np.delete(np.arange(network_parameters[layer_index]), remaining_indices_list[layer_index])
        #no bias balancing as first layer since we don't remove any input neurons, also no bias balancing if no neurons removed from previous layer
        if bias_balancing==False or layer_index==0 or prev_layer_removed_neurons.size==0:
            model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices_list[layer_index+1]])
        else:
            model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices_list[layer_index+1]] + torch.matmul(orig_model['weights'][layer_index][remaining_indices_list[layer_index+1]][:,prev_layer_removed_neurons],torch.from_numpy(layers_mean[layer_index-1][prev_layer_removed_neurons]).float()))
            
    return model

                
        
     

    
    
    
class FC_network_drop_bn(nn.Module):
    def __init__(self, network_parameters, dropout_parameters, use_batchnorm, nonlinearity):
        super().__init__()
        self.nonlinearity = nonlinearity
        self.network_parameters = network_parameters
        self.dropout_parameters = dropout_parameters
        self.use_batchnorm = use_batchnorm
        self.num_layers = len(network_parameters)-1 #number of fc layers
        # using getattr to dynamically access the class corresponding to nonlinearity passed as input
        self.activation_class = getattr(nn, nonlinearity) 
        self.activation = self.activation_class() 
         # using ModuleList, otherwise these layers won't be registered as network parameters
        self.lin = nn.ModuleList()
        self.droplayers = nn.ModuleList()
        if self.use_batchnorm:
            self.bnlayers = nn.ModuleList()
        #we go till num_layers-1 since there is no dropout and batchnorm for last (output) layer
        for i in np.arange(self.num_layers-1): 
            # Adding fully connected network layers
            self.lin.append(nn.Linear(in_features=self.network_parameters[i],out_features=self.network_parameters[i+1])) 
            self.droplayers.append(nn.Dropout(self.dropout_parameters[i]))
            if self.use_batchnorm:
                self.bnlayers.append(nn.BatchNorm1d(self.network_parameters[i+1]))
        #output linear layer
        self.lin.append(nn.Linear(in_features=self.network_parameters[-2],out_features=self.network_parameters[-1])) 
        

    def forward(self,x):
        bs= x.shape[0]
         #Flatten the images. 
        x = x.view(bs,self.network_parameters[0])
        #last layer not included in the following loop because we don't apply nonlinearity there
        for layer_index in np.arange(self.num_layers-1):
            if self.use_batchnorm:
                #we are currently applying batchnorm before the nonlinearity. We can try other way round as well
                x = self.droplayers[layer_index](self.activation(self.bnlayers[layer_index](self.lin[layer_index](x))))
            else:
                x = self.droplayers[layer_index](self.activation(self.lin[layer_index](x)))
                
        x = self.lin[-1](x) 
        return x

    # This function is used to compute the unnormalized joint distribution between individual neuron activations and the target labels. Make sure the model is in eval() mode so that fixed parameters are used for batchnorm. We have removed the dropout layers in computation as they should anyway be identity in eval() mode
    def compute_dist(self,x,target_labels, quantization_levels, layermean=True, mean_multiplier=4):
        bs= x.shape[0] 
        x = x.view(bs,self.network_parameters[0])
        #last layer is not included as we do not apply nonlinearity there.
        for i in np.arange(self.num_layers-1):
            if self.use_batchnorm:
                #we are currently applying batchnorm before the nonlinearity. We can try other way round as well
                x = self.activation(self.bnlayers[i](self.lin[i](x)))
            else:
                x = self.activation(self.lin[i](x))
            #calculating the unnormalized joint distribution
            for k in np.arange(len(quantization_levels)): #iterating over quantizers         
                for j in np.arange(bs): # iterating over examples in each batch
                    #For different nonlinearities we need to define quantizater differently
                    if self.nonlinearity =='Sigmoid':
                        #For sigmoid we have to remove the quantization level from -infty to 0 as sigmoid doesn't go below zero. Also it doesn't go above 1 so we remove the last level. This is why we have [1:-1]
                        quant_bins = np.linspace(0,1,num=quantization_levels[k]+1)[1:-1] 
                        quantization_indices = np.digitize(x[j,:].cpu().numpy(),quant_bins)
                    elif layermean==True:
                        #we have right=True to have deadzone values separated for ReLU from the rest
                        quantization_indices = np.digitize(x[j,:].cpu().numpy(),np.linspace(0,mean_multiplier*self.total_layer_mean[i],num=quantization_levels[k]-1),right=True)
                    else:
                        #we have right=True to have deadzone values separated for ReLU from the rest
                        #each neuron quantized w.r.t. it's own mean
                        quantization_indices = np.array([np.digitize(x[j,l].cpu().numpy(),np.linspace(0,mean_multiplier*self.layers_mean[i][l],num=quantization_levels[k]-1),right=True) for l in np.arange(x.shape[1])])
                        
                    self.layers_P_NY[i][k][(np.arange(x.shape[1]), quantization_indices, [target_labels[j].item()]*x.shape[1])] +=1 # first index on the L.H.S is for accessing the appropriate multidimensional numpy array from the list for the specific layer and the specific quantizer, then the second set of indices on the L.H.S. is for indexing inside this multidimensional array

        #computations for the last layer. Since last layer has no nonlinearity we simply use uniform quantizer spread above and below mean values. These computations are actually not of much use since there is no pruning from output neurons.
        x = self.lin[-1](x) 
        for k in np.arange(len(quantization_levels)):
            for j in np.arange(bs):
                if layermean==True:
                    margin = abs(self.total_layer_mean[self.num_layers-1])*mean_multiplier/2
                    quantization_indices = np.digitize(x[j,:].cpu().numpy(),np.linspace(self.total_layer_mean[-1]-margin,self.total_layer_mean[-1]+margin,num=quantization_levels[k]-1),right=True) 
                else:
                    quantization_indices = np.array([np.digitize(x[j,l].cpu().numpy(),np.linspace(self.layers_mean[-1][l]-abs(self.layers_mean[-1][l])*mean_multiplier/2,self.layers_mean[-1][l]+abs(self.layers_mean[-1][l])*mean_multiplier/2,num=quantization_levels[k]-1),right=True) for l in np.arange(x.shape[1])])
                
                self.layers_P_NY[-1][k][(np.arange(x.shape[1]), quantization_indices,[target_labels[j].item()]*x.shape[1])] +=1 
        return None
    
    #this function computes the mean activation value of each neuron for the trained network. The mean values are useful for both bias-balancing and ReLU quantization. Use the training set to compute the mean values and not the validation/test set. Remember to put the model in eval() mode before computing mean we use fixed parameters for batchnorm layers. Also we have removed dropout computations during mean calculation as they should anyway be srt as identity during eval() mode
    def accumulate_mean(self,x):
        bs= x.shape[0]   
        x = x.view(bs,self.network_parameters[0])  
        for layer_index in np.arange(self.num_layers-1):
            if self.use_batchnorm:
                #we are currently applying batchnorm before the nonlinearity. We can try other way round as well
                x = self.activation(self.bnlayers[layer_index](self.lin[layer_index](x)))
            else:
                x = self.activation(self.lin[layer_index](x))
                
            # calculating the mean values (without any quantization).
            self.layers_mean[layer_index] += np.sum(x.cpu().numpy(),axis=0) 

        x = self.lin[-1](x) 
        self.layers_mean[-1] += np.sum(x.cpu().numpy(),axis=0)
        return None

    
def copy_model_bn(state_dict, network_parameters, dropout_parameters, use_batchnorm, nonlinearity):
    model = FC_network_drop_bn(network_parameters, dropout_parameters, use_batchnorm, nonlinearity) 
    model.load_state_dict(state_dict)
    weights =[]
    biases = []
    if use_batchnorm:
        batchlayers =[]
    num_layers = len(network_parameters)-1
    for layer_index in np.arange(num_layers-1):
        weights.append(model.lin[layer_index].weight.data.clone())
        biases.append(model.lin[layer_index].bias.data.clone())
        if use_batchnorm:
            batchlayers.append({'mean':model.bnlayers[layer_index].running_mean.data.clone(),
                              'var':model.bnlayers[layer_index].running_var.data.clone(),
                              'weight':model.bnlayers[layer_index].weight.data.clone(),
                              'bias': model.bnlayers[layer_index].bias.data.clone()})
            
    weights.append(model.lin[-1].weight.data.clone())
    biases.append(model.lin[-1].bias.data.clone())
    
    returned_dict ={'weights': weights,
                    'biases': biases}
    if use_batchnorm: 
        returned_dict['bnlayers'] = batchlayers
    return returned_dict



    
def single_layer_resize_bn(state_dict, orig_model, layer_index, remaining_indices, device, network_parameters, dropout_parameters, use_batchnorm, nonlinearity, layers_mean=None, bias_balancing=False):
    #initializing the model from scratch so that we don't have residual impacts from previous prunings/retrainings
    model = FC_network_drop_bn(network_parameters, dropout_parameters, use_batchnorm, nonlinearity) 
    model.load_state_dict(state_dict)
    #resizing previous weight matrix and bias for the layer in consideration
    model.lin[layer_index] = nn.Linear(in_features=orig_model['weights'][layer_index].shape[1], out_features=remaining_indices.size)
    model.lin[layer_index].weight = nn.Parameter(orig_model['weights'][layer_index][remaining_indices,:])
    model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices])
    
    if use_batchnorm:
        model.bnlayers[layer_index] = nn.BatchNorm1d(remaining_indices.size)
        model.bnlayers[layer_index].running_mean.data = orig_model['bnlayers'][layer_index]['mean'][remaining_indices].data.clone() 
        model.bnlayers[layer_index].running_var.data = orig_model['bnlayers'][layer_index]['var'][remaining_indices].data.clone() 
        model.bnlayers[layer_index].weight.data = orig_model['bnlayers'][layer_index]['weight'][remaining_indices].data.clone() 
        model.bnlayers[layer_index].bias.data = orig_model['bnlayers'][layer_index]['bias'][remaining_indices].data.clone()
        
        
    #resizing the next weight matrix
    model.lin[layer_index+1] = nn.Linear(in_features= remaining_indices.size, out_features=orig_model['weights'][layer_index+1].shape[0])
    model.lin[layer_index+1].weight = nn.Parameter(orig_model['weights'][layer_index+1][:,remaining_indices])
    
    temp = np.arange(orig_model['biases'][layer_index].shape[0])
    #temp1 is basically same as ordered[:i] in the pruning function
    removed_neurons = np.delete(temp, remaining_indices)
    #no bias balancing needed if no neuron was removed from previous layer
    if bias_balancing==False or removed_neurons.size==0:
        model.lin[layer_index+1].bias = nn.Parameter(orig_model['biases'][layer_index+1])
    else:
        model.lin[layer_index+1].bias = nn.Parameter(orig_model['biases'][layer_index+1] + torch.matmul(orig_model['weights'][layer_index+1][:,removed_neurons],torch.from_numpy(layers_mean[layer_index][removed_neurons]).float()))
    
    
    
    return model

    
    
def resize_network_bn(state_dict, orig_model, remaining_indices_list, device, network_parameters, dropout_parameters, use_batchnorm, nonlinearity, layers_mean=False, bias_balancing=False):
    #initializing the model from scratch so that we don't have residual impacts from previous prunings/retrainings
    model = FC_network_drop_bn(network_parameters, dropout_parameters, use_batchnorm, nonlinearity) 
    model.load_state_dict(state_dict)
    num_layers = len(network_parameters)-1
    for layer_index in np.arange(num_layers):
        model.lin[layer_index] = nn.Linear(in_features=remaining_indices_list[layer_index].size,out_features=remaining_indices_list[layer_index+1].size)
        model.lin[layer_index].weight = nn.Parameter(orig_model['weights'][layer_index][remaining_indices_list[layer_index+1],:][:,remaining_indices_list[layer_index]])
        
        prev_layer_removed_neurons = np.delete(np.arange(network_parameters[layer_index]), remaining_indices_list[layer_index])
        #no bias balancing as first layer since we don't remove any input neurons, also no bias balancing if no neurons removed from previous layer
        if bias_balancing==False or layer_index==0 or prev_layer_removed_neurons.size==0:
            model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices_list[layer_index+1]])
        else:
            model.lin[layer_index].bias = nn.Parameter(orig_model['biases'][layer_index][remaining_indices_list[layer_index+1]] + torch.matmul(orig_model['weights'][layer_index][remaining_indices_list[layer_index+1]][:,prev_layer_removed_neurons],torch.from_numpy(layers_mean[layer_index-1][prev_layer_removed_neurons]).float()))
        
        #there is no batchnorm for the last layer
        if use_batchnorm and layer_index!=num_layers-1:
            model.bnlayers[layer_index] = nn.BatchNorm1d(remaining_indices_list[layer_index+1].size)
            model.bnlayers[layer_index].running_mean.data = orig_model['bnlayers'][layer_index]['mean'][remaining_indices_list[layer_index+1]].data.clone() 
            model.bnlayers[layer_index].running_var.data = orig_model['bnlayers'][layer_index]['var'][remaining_indices_list[layer_index+1]].data.clone() 
            model.bnlayers[layer_index].weight.data = orig_model['bnlayers'][layer_index]['weight'][remaining_indices_list[layer_index+1]].data.clone() 
            model.bnlayers[layer_index].bias.data = orig_model['bnlayers'][layer_index]['bias'][remaining_indices_list[layer_index+1]].data.clone()
            
    return model


def mean_and_std_curves(performance, quantity, low_high, quantizer_index, together, layer_index=None):
    if together==False:
        #in this case we evaluate performance for a specific layer and the performance array contains a dimension corresponding to the layer_index
        combined = np.stack([performance[i]['performance'][quantity,low_high,layer_index,quantizer_index,:] for i in np.arange(len(performance))])
    else:
        #in this case we evaluate performance for the whole network together and the performance array does not contain a dimension corresponding to the layer_index
        combined = np.stack([performance[i]['performance'][quantity,low_high,quantizer_index,:] for i in np.arange(len(performance))])
    
    mean_curve = np.mean(combined,axis=0)
    std_curve  = np.std(combined,axis=0)
    
    return mean_curve,std_curve

def plot_mean_std_curves(curve_list, label_list, ax=None, row=None, col=None, step=None):
    #curve_list is a list of tuples. Each tuple consists of two elements. The first element is a curve consisting of mean values and the second element is a curve consisting of standard deviation values. 
    if step==None:
        step = 1
    color_idx = np.linspace(0, 1, len(curve_list))
    for i in np.arange(len(curve_list)):
        if row==None:
            plt.plot(step*np.arange(np.ceil(curve_list[i][0].size/step)),curve_list[i][0][::step],'-',color=plt.cm.plasma(color_idx[i]), label=label_list[i])
            plt.fill_between(step*np.arange(np.ceil(curve_list[i][0].size/step)), curve_list[i][0][::step] - curve_list[i][1][::step], curve_list[i][0][::step] + curve_list[i][1][::step] , color=plt.cm.plasma(color_idx[i]), alpha=0.2)
        else:
            ax[row,col].plot(step*np.arange(np.ceil(curve_list[i][0].size/step)),curve_list[i][0][::step],'-',color=plt.cm.plasma(color_idx[i]), label=label_list[i])
            ax[row,col].fill_between(step*np.arange(np.ceil(curve_list[i][0].size/step)), curve_list[i][0][::step] - curve_list[i][1][::step], curve_list[i][0][::step] + curve_list[i][1][::step] , color=plt.cm.plasma(color_idx[i]), alpha=0.2)
        
    if row==None:
        plt.legend(loc='lower left')
        plt.show() 
    else: 
        ax[row,col].legend(loc='lower left')
        
    return None

def plot_mean_curves(curve_list, label_list):
    #curve list is a python list. Each element of the list is basically a numpy array representing a mean curve.
    color_idx = np.linspace(0, 1, len(curve_list))
    for i in np.arange(len(curve_list)):
        plt.plot(np.arange(curve_list[i].size),curve_list[i],'-',color=plt.cm.plasma(color_idx[i]), label=label_list[i])
        
    plt.legend(loc='lower left')
    plt.show()
    
    return None

def combined_violinplot(results, quantity, quantizer_index, ax=None, row=None, col=None):
    #results are directly the results stored from training code. quantity is a string identifying the dictionary item corresponding to the quantity for making violin plots and quantizer_index defines how many quantization levels were used to define the joint distribution. This function combines the quantity values for a given layers for all the random experiments together to plot
    num_hidden_layers = len(results[0]['network_paramaters'])-2
    aggreg_layer_values =[]
    for layer_index in np.arange(num_hidden_layers):
        aggreg_layer_values.append(np.concatenate([results[exp_index][quantity][layer_index][quantizer_index] for exp_index in np.arange(len(results))]))
    
    if row==None:
        plt.violinplot(aggreg_layer_values, showmeans=True, showextrema=True, showmedians=True)
    else:
        ax[row,col].violinplot(aggreg_layer_values, showmeans=True, showextrema=True, showmedians=True)
        
    return None

def individual_violinplot(results, quantity, quantizer_index, experiment_index=None):
    #results are directly the results stored from training code. quantity is a string identifying the dictionary item corresponding to the quantity for making violin plots and quantizer_index defines how many quantization levels were used to define the joint distribution. This function displays the violin plot for an individual experiment
    num_hidden_layers = len(results[0]['network_paramaters'])-2
    if experiment_index==None:
        experiment_index = np.random.randint(low=0, high=len(results))
    layer_values =[]
    for layer_index in np.arange(num_hidden_layers):
        layer_values.append(results[experiment_index][quantity][layer_index][quantizer_index])
    
    plt.violinplot(layer_values, showmeans=True, showextrema=True, showmedians=True)
    return None


def single_neuron_hist(results, layer_index, quantizer_index, neuron_index=None, experiment_index=None):
    """The function plots the distribution histogram of a specific neuron from a specific layer for a specific experiment over quantization levels of the validation set samples to show how many samples fell into which bin"""
    if experiment_index==None:
        experiment_index = np.random.randint(low=0, high=len(results))
    if neuron_index==None:
        neuron_index = np.random.randint(low=0, high=results[experiment_index]['layers_mean'][layer_index].size)
    #extracting the marginal distribution of the specific neuron over the quantization bins
    joint_count = results[experiment_index]['layers_P_NY'][layer_index][quantizer_index][neuron_index,:,:]
    marginal_quantizer_dist = np.sum(joint_count,axis=1)/np.sum(joint_count)
    
    #generating the bar graph
    index = np.arange(marginal_quantizer_dist.size)
    plt.bar(index, marginal_quantizer_dist)
    
    #generating the x_ticks. Since the quantization mechanisms are different for different nonlinearities and if layermean was used we have to define a different way to calculate the x_ticks as well
    num_quantization_levels = results[experiment_index]['quantization_levels'][quantizer_index]
    if results[experiment_index]['non_linearity']=='Sigmoid':
        x_values = np.linspace(0,1,num=num_quantization_levels+1)[1:] 
    elif results[experiment_index]['non_linearity']=='ReLU':
        mean_multiplier= results[experiment_index]['mean_multiplier']
        if results[experiment_index]['use_layermean']==True:
            total_layer_mean= results[experiment_index]['total_layer_mean'][layer_index] 
            x_values = []
            x_values.extend(np.around(np.linspace(0,mean_multiplier*total_layer_mean, num_quantization_levels-1), decimals=2))
            x_values.extend([np.inf]) 
        else:
            neuron_mean = results[experiment_index]['layers_mean'][layer_index][neuron_index]
            x_values = []
            x_values.extend(np.around(np.linspace(0,mean_multiplier*neuron_mean, num_quantization_levels-1), decimals=2))
            x_values.extend([np.inf]) 
    
    
    plt.xticks(index, x_values)
    plt.show()
    return None